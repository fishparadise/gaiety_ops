# Gaiety_ops

#### 介绍
Gaiety 的 OPS端
Gaiety是我在博彦做的最后一个项目，现在看来当年的思路以及一些地方存在很多问题
于是打算用新的技术重新设计一遍，这个项目只包含后端接口

#### 软件架构
软件架构说明
多租户模型下，涵盖所有常见的类美团类APP的功能
这个是OPS端，包含如下功能：
* 租户的添加、停用、清理
* 租户Feature启停
* 租户的层级处理
* 租户报表功能
* 数据批处理功能
* Inspector 运维功能
* 定时任务配置
* 其余功能后续再说


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
