package com.jerry.ops.bo;

import com.jerry.ops.config.tenant.TenantRole;
import lombok.*;

/**
 * 租户
 *
 * @author : Fish Paradise
 * @version 1.0
 * @date : 2021-10-30 16:38
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TenantBo {
    String tenantName;
    Integer role;
}
