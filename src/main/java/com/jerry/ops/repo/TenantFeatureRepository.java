package com.jerry.ops.repo;

import com.jerry.ops.config.tenant.Feature;
import com.jerry.ops.model.TenantFeature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 租户功能表
 *
 * @author : Fish Paradise
 * @version 1.0
 * @date : 2021-10-28 20:32
 */
@Repository
public interface TenantFeatureRepository extends JpaRepository<TenantFeature, Long> {

    /**
     * .
     * @param tenantId .
     * @return .
     */
    Streamable<TenantFeature> findAllByTenantId(Long tenantId);

    /**
     * .
     * @param featureList .
     * @return .
     */
    Streamable<TenantFeature> findAllByFeatureIn(List<Feature> featureList);
}
