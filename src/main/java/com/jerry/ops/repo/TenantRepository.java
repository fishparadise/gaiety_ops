package com.jerry.ops.repo;

import com.jerry.ops.model.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

/**
 * 租户处理数据
 *
 * @author : Fish Paradise
 * @version 1.0
 * @date : 2021-10-28 18:47
 */
@Repository
public interface TenantRepository extends JpaRepository<Tenant, Long> {

    /**
     * find by tenantName
     * @param tenantName .
     * @return stream of find by tenantName
     */
    Streamable<Tenant> findAllByTenantName(String tenantName);
}
