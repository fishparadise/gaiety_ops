package com.jerry.ops.controller;

import com.jerry.ops.bo.TenantBo;
import com.jerry.ops.service.TenantService;
import com.jerry.ops.vo.TenantVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

/**
 * 租户控制器
 * Todo 先设计为mapping映射，后期把/app/v1/全部改为网关映射
 *
 * @author : Fish Paradise
 * @version 1.0
 * @date : 2021-10-30 16:32
 */
@RestController
@Slf4j
@RequestMapping("/app/v1/tenant")
public class TenantController {

    @Autowired
    TenantService tenantService;

    @PostMapping("/add")
    public TenantVo addTenant(@RequestBody TenantBo tenant){
        return tenantService.addTenant(tenant);
    }

    @DeleteMapping
    public boolean deleteTenant(@RequestBody Long tenantId){
        return tenantService.deleteTenant(tenantId);
    }

    @PostMapping("/purge")
    public void purgeTenant(){

    }
}
