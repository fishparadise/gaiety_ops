package com.jerry.ops.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 租户信息表
 *
 * @author : Fish Paradise
 * @version 1.0
 * @date : 2021-10-28 17:57
 */
@Entity
@Builder
@ToString(callSuper = true)
@Setter
@Getter
@Table(name = "T_TENANT")
@NoArgsConstructor
@AllArgsConstructor
public class Tenant extends BaseEntity implements Serializable {
    private String tenantName;
}
