package com.jerry.ops.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 基础Entity
 *
 * @author : Fish Paradise
 * @version 1.0
 * @date : 2021-10-28 18:04
 */
@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BaseEntity implements Serializable {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(updatable = false)
    @CreationTimestamp
    private Date createDate;

    @UpdateTimestamp
    private Date updateDate;

    private Date deleteDate;

    private Boolean active = true;
}
