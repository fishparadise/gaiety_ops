package com.jerry.ops.model;

import com.jerry.ops.config.tenant.Feature;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 每个租户的功能表
 *
 * @author : Fish Paradise
 * @version 1.0
 * @date : 2021-10-28 20:07
 */
@Entity
@Table(name = "T_TENANT_FEATURE")
@NoArgsConstructor
public class TenantFeature extends BaseEntity implements Serializable {
    @Getter
    private Long tenantId;

    @Getter
    @Enumerated(EnumType.STRING)
    private Feature feature;

    public TenantFeature(Long tenantId, Feature feature) {
        this.tenantId = tenantId;
        this.feature = feature;
    }
}
