package com.jerry.ops.service;

import com.jerry.ops.bo.TenantBo;
import com.jerry.ops.vo.TenantVo;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * 租户服务
 *
 * @author : Fish Paradise
 * @version 1.0
 * @date : 2021-10-30 16:42
 */
@Component
public interface TenantService {

    TenantVo addTenant(TenantBo tenantBo);

    Boolean deleteTenant(Long tenantId);
}
