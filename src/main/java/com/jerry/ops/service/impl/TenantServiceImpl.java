package com.jerry.ops.service.impl;

import com.alibaba.fastjson.JSON;
import com.jerry.ops.bo.TenantBo;
import com.jerry.ops.model.Tenant;
import com.jerry.ops.repo.TenantFeatureRepository;
import com.jerry.ops.repo.TenantRepository;
import com.jerry.ops.service.TenantService;
import com.jerry.ops.vo.TenantVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 租户服务实现
 *
 * @author : Fish Paradise
 * @version 1.0
 * @date : 2021-10-30 16:43
 */
@Service
@Slf4j
public class TenantServiceImpl implements TenantService {

    private final TenantRepository tenantRepository;

    private final TenantFeatureRepository tenantFeatureRepository;

    public TenantServiceImpl(TenantRepository tenantRepository,
                             TenantFeatureRepository tenantFeatureRepository) {
        this.tenantRepository = tenantRepository;
        this.tenantFeatureRepository = tenantFeatureRepository;
    }

    @Override
    public TenantVo addTenant(TenantBo tenantBo) {
        log.info(JSON.toJSONString(tenantBo));
        var tenant = new Tenant(tenantBo.getTenantName());
        tenantRepository.save(tenant);
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteTenant(Long tenantId) {
        var tenant = tenantRepository.findById(tenantId).orElseThrow();
        tenant.setActive(false);
        tenantRepository.save(tenant);
        return false;
    }
}
