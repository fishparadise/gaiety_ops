package com.jerry.ops.config.tenant;

/**
 * 租住权限
 *
 * @author : Fish Paradise
 * @version 1.0
 * @date : 2021-10-30 16:40
 */
public enum TenantRole {
    /**
     * 超级租户
     */
    Super(0),
    /**
     * 管理员租户
     */
    Admin(1),

    /**
     * 普通租户
     */
    ROLE(2)

    ;

    Integer role;

    TenantRole(Integer role){
        this.role = role;
    }

    public static TenantRole getTenant(Integer role){
        for (TenantRole t:TenantRole.values()){
            if (t.role.equals(role)) {
                return t;
            }
        }
        return null;
    }
}
