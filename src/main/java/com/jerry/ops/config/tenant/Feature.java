package com.jerry.ops.config.tenant;


import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 功能
 *
 * @author : Fish Paradise
 * @version 1.0
 * @date : 2021-10-28 18:22
 */
public enum Feature {
    /**
     * 默认功能
     */
    DEFAULT_FEATURE("default feature", "default feature", FeatureMode.GA),

    /**
     * 开发功能
     */
    DEV_FEATURE("Dev feature", "dev feature", FeatureMode.DEV)
    ;

    /**
     * feature name
     */
    @Getter
    private final String featureName;

    /**
     * feature description
     */
    @Getter
    private final String description;

    /**
     * feature mode
     */
    @Getter
    private final FeatureMode featureMode;

    Feature(String featureName, String description, FeatureMode featureMode) {
        this.featureName = featureName;
        this.description = description;
        this.featureMode = featureMode;
    }

    public static List<Feature> getAllGaFeature(){
        return Stream.of(Feature.values())
                .filter(
                        feature ->
                                FeatureMode.GA.equals(feature.getFeatureMode()))
                .collect(Collectors.toList());
    }

    public enum FeatureMode{
        /**
         * 开发阶段
         */
        DEV,

        /**
         * 测试阶段
         */
        TEST,

        /**
         * 生产
         */
        PRODUCTION,

        /**
         * GA
         */
        GA
        ;

        FeatureMode() {
        }
    }
}
