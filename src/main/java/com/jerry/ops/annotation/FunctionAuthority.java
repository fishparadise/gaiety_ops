package com.jerry.ops.annotation;

import java.lang.annotation.*;

/**
 * 函数权限校验
 *
 * @author : Fish Paradise
 * @version 1.0
 * @date : 2021-10-30 22:26
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FunctionAuthority {
}
